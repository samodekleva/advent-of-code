#! /usr/bin/env elixir

defmodule Day10Puzzle1 do
	
	def parse_star(str) do
		matches = Regex.named_captures(~r/position=<\s?(?<x>\-?\d*),\s*(?<y>\-?\d*)>\s*velocity=<\s?(?<dx>\-?\d*),\s*(?<dy>\-?\d*)>/, str)
		%{ x: String.to_integer(matches["x"]), y: String.to_integer(matches["y"]), dx: String.to_integer(matches["dx"]), dy: String.to_integer(matches["dy"]) }
	end

	def parse_stars(list) do
		Enum.map(list, fn x -> parse_star(x) end)
	end

	def time_passing(stars) do
		Enum.map(stars, fn n -> %{ x: n[:x] + n[:dx], y: n[:y] + n[:dy], dx: n[:dx], dy: n[:dy] } end)
	end 

	def count(stars) do
		count(time_passing(stars), distance(stars), 1)	
	end

	def count(stars, last_distance, n) do
		star_distance = stars
		|> time_passing()
		|> distance()

		if (star_distance < last_distance) do
			count(time_passing(stars), star_distance, n+1)
		else
			output(stars)
			n
		end
	end

	def distance(stars) do
		
		xavg = avg(stars, :x)
		yavg = avg(stars, :y)

		List.foldr(stars, 0, fn n, acc -> :math.sqrt(:math.pow((n[:x]-xavg),2) + :math.pow((n[:y]-yavg),2)) + acc end) / Enum.count(stars)-1

	end

	defp avg(stars, dimension) do
		Enum.map(stars, fn x -> x[dimension] end)
		|> Enum.sum()
		|> Kernel./(Enum.count(stars))
	end

	def output(list) do

		xmin = Enum.min(Enum.map(list, fn x -> x[:x] end)) - 1
		xmax = Enum.max(Enum.map(list, fn x -> x[:x] end)) + 1
		ymin = Enum.min(Enum.map(list, fn x -> x[:y] end)) - 1
		ymax = Enum.max(Enum.map(list, fn x -> x[:y] end)) + 1

		for i <-0..(ymax - ymin) do
			row = stars_in_row(list, i, ymin, xmin, xmax)	
			IO.inspect(row)
		end

		list
	end

	def stars_in_row(stars, row, offset, xmin, xmax) do
		stars_found = Enum.filter(stars, fn n -> n[:y] == row + offset end)
		|> Enum.map(fn n -> { :x, n[:x] } end) 
			
		for i <- 0..(xmax - xmin) do
			if (List.keyfind(stars_found, i + xmin, 1)) do
				?X
			else 
				?.
			end
		end
	end

end

File.read!('input.txt')
|> String.split("\n")
|> Day10Puzzle1.parse_stars()
|> Day10Puzzle1.count()
|> IO.inspect()


ExUnit.start()

defmodule Day3Puzzle1Test do
	use ExUnit.Case

	import Day10Puzzle1

	test "parsing stars" do
		assert parse_star("position=< 8, -2> velocity=< 0,  1>") == %{dx: 0, dy: 1, x: 8, y: -2}
		assert parse_star("position=<15,  0> velocity=<-2,  0>") == %{dx: -2, dy: 0, x: 15, y: 0}
	end

end
