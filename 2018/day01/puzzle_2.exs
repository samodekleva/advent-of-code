# Run it with: elixir puzzle_02.exs
# https://adventofcode.com/2018/day/1

defmodule Puzzle2 do

  def freq(inputs) do
    freq(inputs, 0, MapSet.new([0]))
  end

  def freq([], result, _), do: result

  def freq([head | tail], result, results) do
    newresult = result + String.to_integer(head)
    if (MapSet.member?(results, newresult)) do
      freq([], newresult, results)
    else
      freq(tail ++ [head], newresult, MapSet.put(results, newresult))
    end
  end

  def calc() do
    File.read!("input.txt")
    |> String.split()
    |> freq
  end
end

#IO.puts(Puzzle2.freq(["-6", "+3", "+8", "+5", "-6"]))
IO.puts(Puzzle2.calc())
ExUnit.start()

defmodule Puzzle2Test do
  use ExUnit.Case

  import Puzzle2

  test "special cases" do
    assert freq([]) == 0
  end

  test "regular cases" do
    assert freq(["+1", "-1"]) == 0
    assert freq(["+3", "+3", "+4", "-2", "-4"]) == 10
    assert freq(["-6", "+3", "+8", "+5", "-6"]) == 5
    assert freq(["+7", "+7", "-2", "-7", "-4"]) == 14
  end
end
