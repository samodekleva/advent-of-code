# Run it with: elixir puzzle_01.exs
# https://adventofcode.com/2018/day/1

defmodule Puzzle1 do
  def freq(list), do: List.foldl(list, 0, fn x, acc -> String.to_integer(x) + acc end)

  def calc() do
    File.read!("input.txt")
    |> String.split()
    |> freq
  end
end

IO.puts(Puzzle1.calc())
ExUnit.start()

defmodule Puzzle1Test do
  use ExUnit.Case

  import Puzzle1

  test "special cases" do
    assert freq([]) == 0
  end

  test "regular cases" do
    assert freq(["+1", "+1", "+1"]) == 3
    assert freq(["+1", "+1", "-2"]) == 0
    assert freq(["-1", "-2", "-3"]) == -6
  end
end
