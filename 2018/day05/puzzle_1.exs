#! /usr/bin/env elixir

defmodule Day05Puzzle1 do
  def reducer(str) do
    list = String.to_charlist(str)
    reducer(Enum.take(list, 1), Enum.drop(list, 1), [])
  end

  def reducer(current, [next|future], [previous]) do
    if (compare(current, [next])) do
        check(current, [next], [previous] ++ future)
        reducer([previous], future, [])
    else
      reducer([next], future, current ++ [previous])
    end
  end


  def reducer(current, [next|future], [previous|past]) do
    if (compare(current, [next])) do
        check(current, [next], [previous] ++ future ++ past)
        reducer([previous], future, past)
    else
      reducer([next], future, current ++ [previous] ++ past)
    end
  end

  # def reducer(current, [next], [previous|past]) do
  #   if (compare(current, [next])) do
  #     check(current, [next], [previous] ++ past)
  #     reducer([previous], [], past)
  #   else
  #     reducer([next], [], current ++ [previous] ++ past)
  #   end
  # end

  def reducer(current, [next|future], []) do
    if (compare(current, [next])) do
      check(current, [next], future)
      reducer(Enum.take(future, 1), Enum.drop(future, 1), [])
    else
      reducer([next], future, current)
    end
  end


  def reducer(current, [], [previous|past]) do
    Enum.reverse(current ++ [previous] ++ past)
    |>List.to_string()
  end

  defp check(current, next, rest) do
    # IO.inspect([current] ++ [next])
    # IO.inspect(Enum.count(rest))
  end

  defp compare(current, next) do
    cond do
      current == next -> false
      String.downcase(List.to_string(current)) == List.to_string(next) -> true
      String.upcase(List.to_string(current)) == List.to_string(next) -> true
      true -> false
    end
  end

end

IO.inspect(File.read!("input.txt")
|> String.to_charlist()
|> Enum.count())

IO.inspect(File.read!("input.txt")
|> Day05Puzzle1.reducer()
|> Day05Puzzle1.reducer()
|> Day05Puzzle1.reducer()
|> String.to_charlist()
|> Enum.count())

ExUnit.start()

defmodule Day05Puzzle1Test do
  use ExUnit.Case

  test "reducer" do
    assert Day05Puzzle1.reducer("dabAcCaCBAcCcaDA") == "dabCBAcaDA"
  end


end
