#! /usr/bin/env elixir

defmodule ChocolateCharts do

  def new_recipes(elf1, elf2, recipes) do
    new_recipes =
      Enum.at(recipes, elf1) + Enum.at(recipes, elf2)
      |> Integer.to_string()
      |> String.split("")
      |> Enum.filter(fn x -> String.length(x) > 0 end)
      |> Enum.map(fn x -> String.to_integer(x) end)
    recipes ++ new_recipes
  end

  def new_elf_position(position, recipes) do
    rem(Enum.at(recipes, position) + position + 1, length(recipes))
  end

  def cycle(steps, _, _, recipes) when (length(recipes) > (steps+10)) do
    leftovers = Enum.drop(recipes, steps)
    Enum.take(leftovers, 10)
  end

  def cycle(steps, elf1, elf2, recipes) do
    recipes = ChocolateCharts.new_recipes(elf1,elf2,recipes)
    elf1 = ChocolateCharts.new_elf_position(elf1, recipes)
    elf2 = ChocolateCharts.new_elf_position(elf2, recipes)
    cycle(steps, elf1, elf2, recipes)
  end

  def is_pattern(recipes, pattern) do
    :binary.match(List.to_string(recipes), pattern)
  end

  def cycle2(pattern, elf1, elf2, recipes) do
    recipes = ChocolateCharts.new_recipes(elf1,elf2,recipes)
    elf1 = ChocolateCharts.new_elf_position(elf1, recipes)
    elf2 = ChocolateCharts.new_elf_position(elf2, recipes)
    match = is_pattern(recipes, pattern)
    if (match != :nomatch) do
      { steps, _} = match
      steps
    else
      cycle2(pattern, elf1, elf2, recipes)
    end
  end


end


ExUnit.start()

defmodule ChocolateChartsTest do
  use ExUnit.Case

  import ChocolateCharts

  test "cycles" do
    assert cycle(9, 0, 1, [3, 7]) == [5, 1, 5, 8, 9, 1, 6, 7, 7, 9]
    assert cycle(2018, 0, 1, [3, 7]) == [5, 9, 4, 1, 4, 2, 9, 8, 8, 2]
  end

  test "pattern" do
    assert is_pattern([0,1,3,7,8,0,4], <<7,8,0>>) == {3, 3}
  end

  test "pattern finding" do
    assert cycle2(<<5,1,5,8,9>>, 0, 1, [3, 7]) == 9
    assert cycle2(<<5,9,4,1,4>>, 0, 1, [3, 7]) == 2018
  end

end

# ChocolateCharts.cycle(47801, 0, 1, [3, 7])
# |> IO.inspect()

ChocolateCharts.cycle2(<<0,4,7,8,0,1>>, 0, 1, [3, 7])
|> IO.inspect()
