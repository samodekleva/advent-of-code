# Run it with: elixir puzzle_01.exs
# https://adventofcode.com/2018/day/1

defmodule Day2Puzzle2 do

  def check_diffs([head | tail]) do
	check_diffs(head, tail, tail)  	
  end

  def check_diffs(first, [second | tail], rest) do
  	case isOneOff(first, second) do
  		{ :ok, a} -> { :ok, a}
  		{ :noreply } -> check_diffs(first, tail, rest)
  	end
  end

  def check_diffs(_, [], []) do
  	{ :ok, :noreply }
  end

  def check_diffs(_, [], rest) do
  	check_diffs(rest)
  end

  defp isOneOff(first, second) do
  	diff = String.myers_difference(first, second)
	same = List.foldr(diff, "", fn (x, acc) -> 
  	  case x do
  	    { :eq, a } ->  a <> acc
  	    _ -> acc 
  	  end
	end)
	if (String.length(same) + 1 == String.length(first)) do
		{ :ok, same}
	else
		{ :noreply }
	end
  end

end

IO.inspect(File.read!("input.txt")
|>String.split()
|>Day2Puzzle2.check_diffs())


# Day2Puzzle2.check_diffs(ids)

ExUnit.start()

defmodule Day2Puzzle1Test do
  use ExUnit.Case

  import Day2Puzzle2

  test "frequencies" do
    ids = ["abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz"]
    assert check_diffs(ids) == { :ok, "fgij" }
  end
end

