# Run it with: elixir puzzle_01.exs
# https://adventofcode.com/2018/day/1

defmodule Day2Puzzle1 do
  def frequencies(word) do
    chars = to_charlist(word)
    List.foldl(chars, %{}, fn(char, acc) -> Map.update(acc, List.to_atom([char]), 1, &(&1 + 1)) end)
  end
  
  def count(rows, value) do
    if (value in Map.values(rows)) do
      1
    else 
      0
    end
  end

  def get(list, key) do
    if Map.get(list, key, 0) do 1 else 0 end
  end

  def calc(rows, value) do
    String.split(rows)
    |> Enum.map(fn x -> frequencies(x) end)
    |> Enum.map(fn x -> count(x, value) end)
    |> Enum.sum()
  end

  def process(rows) do
    IO.inspect(calc(rows, 2) * calc(rows, 3))
  end

end

File.read!("input.txt")
|> Day2Puzzle1.process()

ExUnit.start()

defmodule Day2Puzzle1Test do
  use ExUnit.Case

  import Day2Puzzle1

  test "frequencies" do
    assert frequencies('') == %{}
    assert frequencies('abcdef') == %{ a: 1, b: 1, c: 1, d: 1, e: 1, f: 1 } 
    assert frequencies('bababc') ==  %{ a: 2, b: 3, c: 1 } 
    assert frequencies('abbcde') == %{ a: 1, b: 2, c: 1, d: 1, e: 1 } 
    assert frequencies('abcccd') == %{ a: 1, b: 1, c: 3, d: 1 } 
    assert frequencies('aabcdd') == %{ a: 2, b: 1, c: 1, d: 2 } 
    assert frequencies('abcdee') == %{ a: 1, b: 1, c: 1, d: 1,  e: 2 } 
    assert frequencies('ababab') == %{ a: 3, b: 3 } 
  end

  test "sums" do
    assert calc("abcdef\nbababc\nabbcde\nabcccd\naabcdd\nabcdee\nababab", 2) == 4
    assert calc("abcdef\nbababc\nabbcde\nabcccd\naabcdd\nabcdee\nababab", 3) == 3
  end
end

