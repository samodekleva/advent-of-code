#! /usr/bin/env elixir

defmodule Day12 do
  def load_initial_state(file) do
    [initial_state] = File.read!(file)
    |> String.split("\n")
    |> Enum.take(1)
    initial_state_parse(initial_state)
  end

  def breed_generations(pattern, state, generations) when (generations == 1) do
    matches = match_pattern(pattern, state)
    # IO.inspect( %{ line: generate_line(matches), offset: Enum.min(matches)})
    checksum(matches)
  end

  def breed_generations(pattern, state, generations) do
    matches = match_pattern(pattern, state)
    state = %{ line: generate_line(matches), offset: Enum.min(matches)}
    # IO.inspect(state)
    breed_generations(pattern, state, generations - 1)
  end

  def checksum(patterns) do
    Enum.sum(patterns)
  end

  def breed(patterns, state) do
    matches = match_pattern(patterns, state)
    %{ line: generate_line(matches), offset: Enum.min(matches)}
  end

  def generate_line(matches) do
    line = for i <- Enum.min(matches)..Enum.max(matches) do
      if (Enum.member?(matches, i)) do
        '#'
      else
        '.'
      end
    end
    List.to_string(line)
  end

  def match_pattern(patterns, state) do
    match_pattern(patterns, "....." <> state[:line] <> ".....", state[:offset] - 5, 0, [])
  end

  def match_pattern(patterns, line, offset, start, acc) do
    match = :binary.match(line, patterns, [{ :scope, { start, String.length(line) - start }}])
    case (match) do
      :nomatch -> acc
      {location, _} -> match_pattern(patterns, line, offset, location + 1, [location + 2 + offset | acc])
    end
  end

  def load_patterns(file) do
    File.read!(file)
    |> String.split("\n")
    |> Enum.drop(2)
    |> Enum.map(fn x -> line_parse(x) end)
    |> Enum.filter(fn x -> x[:plant] == "#" end)
  end

  def initial_state_parse(str) do
    "initial state: " <> line = str
    %{ line: line, offset: 0 }
  end

  def line_parse(str) when str == "", do: nil

  def line_parse(str) do
    [pattern | plant ] = String.split(str, " => ")
    %{ pattern: pattern, plant: plant |> List.last() }
  end

end

ExUnit.start()

defmodule Day12Test do
  use ExUnit.Case

  import Day12

  test "parsing" do
    assert initial_state_parse("initial state: #..#.#..##......###...###") == %{ line: "#..#.#..##......###...###", offset: 0 }
    assert line_parse("..#.. => #") == %{ pattern: "..#..", plant: "#" }
    assert line_parse("..... => .") == %{ pattern: ".....", plant: "." }

  end

  test "reading file" do

    assert load_initial_state("input-test.txt") == %{ line: "#..#.#..##......###...###", offset: 0 }
    assert load_patterns("input-test.txt") == [
      %{pattern: "...##", plant: "#"},
      %{pattern: "..#..", plant: "#"},
      %{pattern: ".#...", plant: "#"},
      %{pattern: ".#.#.", plant: "#"},
      %{pattern: ".#.##", plant: "#"},
      %{pattern: ".##..", plant: "#"},
      %{pattern: ".####", plant: "#"},
      %{pattern: "#.#.#", plant: "#"},
      %{pattern: "#.###", plant: "#"},
      %{pattern: "##.#.", plant: "#"},
      %{pattern: "##.##", plant: "#"},
      %{pattern: "###..", plant: "#"},
      %{pattern: "###.#", plant: "#"},
      %{pattern: "####.", plant: "#"}
    ]
  end

  test "breeding" do
    patterns = Day12.load_patterns("input-test.txt")
    |> Enum.map(fn x -> x[:pattern] end)

    state = Day12.load_initial_state("input-test.txt")

    assert Day12.breed_generations(patterns, state, 20) == 325
  end


end

patterns = Day12.load_patterns("input.txt")
|> Enum.map(fn x -> x[:pattern] end)

state = Day12.load_initial_state("input.txt")

Day12.breed_generations(patterns, state, 20)
|> IO.inspect()

# state199 = %{
#   line: "##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##",
#   offset: 172
# }

state50bminusone = %{
  line: "##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##...##",
  offset: (50_000_000_000 - 200 + 172)
}

Day12.breed_generations(patterns, state50bminusone, 1)
|> IO.inspect()
