#! /usr/bin/env elixir

defmodule Cloth do
	def init(x \\ 1000, y \\ 1000) do
		empty_row = Map.new( for i <- 0..(x-1), do: { i, nil })
		map = Map.new(for n <- 0..(y-1), do: { n, empty_row })
		map = Map.put(map, :x, x);
		Map.put(map, :y, y)
	end

	def handle_claims([claim | claims], cloth) do
		cloth = handle_claim(cloth, Claim.init(claim))
		handle_claims(claims, cloth)
	end

	def handle_claims([], cloth) do
		count_x(cloth)
	end

	def handle_claim(cloth, claim) do
		new_cloth = Map.new(for i <- 0..(cloth.y-1), do: { i, if ((i >= claim.y) && (i < (claim.y + claim.h))) do
					handle_claim_row(Map.get(cloth, i), cloth.x, claim) 
				else 
					Map.get(cloth, i)
				end
			})
		new_cloth = Map.put(new_cloth, :x, cloth.x)
		Map.put(new_cloth, :y, cloth.y)
	end

	def to_list(cloth) do
		for i <- 0..(cloth.x-1) do
		  Map.values(Map.get(cloth, i))
		end
	end

	def count_x(cloth) do
		to_list(cloth)
		|> Enum.map(fn x -> Enum.count(x, fn i -> (i == "X") end) end)
		|> Enum.sum()
	end

	defp handle_claim_row(row, x, claim) do
	 	Map.new(for n <- 0.. (x-1) do
	 		if ((n >= claim.x ) and (n < (claim.x + claim.w))) do
				if (Map.get(row, n) == nil) do
		 			{ n, claim.id}
				else  
					{ n, "X" }
		 		end
	 		else
	 			{ n, Map.get(row, n)}
	 		end 	 	
	 	end)
	end

end

defmodule Claim do
	def init(str) do
		matches = Regex.named_captures(~r/#(?<id>\d*)\s@\s(?<x>\d*),(?<y>\d*)\:\s(?<w>\d*)x(?<h>\d*)/, str)
		claim = Map.new()
		claim = Map.put_new(claim, :id, String.to_integer(matches["id"]))
		claim = Map.put_new(claim, :x, String.to_integer(matches["x"]))
		claim = Map.put_new(claim, :y, String.to_integer(matches["y"]))
		claim = Map.put_new(claim, :w, String.to_integer(matches["w"]))
		claim = Map.put_new(claim, :h, String.to_integer(matches["h"]))
		claim
	end
end

File.read!("input.txt") 
|> String.split("\n")
|> Enum.filter(fn x -> x <> "\n" end)
|> Cloth.handle_claims(Cloth.init())
|> IO.inspect()

ExUnit.start()

defmodule Day3Puzzle1Test do
  use ExUnit.Case

  test "cloth" do
  	sample_10x10 = %{
			  0 => %{
			    0 => nil,
			    1 => nil,
			    2 => nil,
			    3 => nil,
			    4 => nil,
			    5 => nil,
			    6 => nil,
			    7 => nil,
			    8 => nil,
			    9 => nil
			  },
			  1 => %{
			    0 => nil,
			    1 => nil,
			    2 => nil,
			    3 => nil,
			    4 => nil,
			    5 => nil,
			    6 => nil,
			    7 => nil,
			    8 => nil,
			    9 => nil
			  },
			  2 => %{
			    0 => nil,
			    1 => nil,
			    2 => nil,
			    3 => nil,
			    4 => nil,
			    5 => nil,
			    6 => nil,
			    7 => nil,
			    8 => nil,
			    9 => nil
			  },
			  3 => %{
			    0 => nil,
			    1 => nil,
			    2 => nil,
			    3 => nil,
			    4 => nil,
			    5 => nil,
			    6 => nil,
			    7 => nil,
			    8 => nil,
			    9 => nil
			  },
			  4 => %{
			    0 => nil,
			    1 => nil,
			    2 => nil,
			    3 => nil,
			    4 => nil,
			    5 => nil,
			    6 => nil,
			    7 => nil,
			    8 => nil,
			    9 => nil
			  },
			  5 => %{
			    0 => nil,
			    1 => nil,
			    2 => nil,
			    3 => nil,
			    4 => nil,
			    5 => nil,
			    6 => nil,
			    7 => nil,
			    8 => nil,
			    9 => nil
			  },
			  6 => %{
			    0 => nil,
			    1 => nil,
			    2 => nil,
			    3 => nil,
			    4 => nil,
			    5 => nil,
			    6 => nil,
			    7 => nil,
			    8 => nil,
			    9 => nil
			  },
			  7 => %{
			    0 => nil,
			    1 => nil,
			    2 => nil,
			    3 => nil,
			    4 => nil,
			    5 => nil,
			    6 => nil,
			    7 => nil,
			    8 => nil,
			    9 => nil
			  },
			  8 => %{
			    0 => nil,
			    1 => nil,
			    2 => nil,
			    3 => nil,
			    4 => nil,
			    5 => nil,
			    6 => nil,
			    7 => nil,
			    8 => nil,
			    9 => nil
			  },
			  9 => %{
			    0 => nil,
			    1 => nil,
			    2 => nil,
			    3 => nil,
			    4 => nil,
			    5 => nil,
			    6 => nil,
			    7 => nil,
			    8 => nil,
			    9 => nil
			  },
			  :x => 10,
  			:y => 10
		}
  	assert Cloth.init(10, 10) == sample_10x10
	end

	test "claim"  do
		assert Claim.init("#1 @ 1,3: 4x4") == %{id: 1, x: 1, y: 3, w: 4, h: 4}
		assert Claim.init("#2 @ 3,1: 4x4") == %{id: 2, x: 3, y: 1, w: 4, h: 4}
		assert Claim.init("#3 @ 5,5: 2x2") == %{id: 3, x: 5, y: 5, w: 2, h: 2}
	end

	test "handle_claim" do
		cloth = Cloth.init(10, 10)
		cloth = Cloth.handle_claim(cloth, Claim.init("#1 @ 1,3: 4x4"))
		IO.inspect(Cloth.to_list(cloth))
		cloth = Cloth.handle_claim(cloth, Claim.init("#2 @ 3,1: 4x4"))
		IO.inspect(Cloth.to_list(cloth))
		cloth = Cloth.handle_claim(cloth, Claim.init("#3 @ 5,5: 2x2"))
		IO.inspect(Cloth.to_list(cloth))
		assert Cloth.count_x(cloth) == 4
	end

	test "handle_claims" do
		cloth = Cloth.init(10, 10)
		claims = [ "#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2" ]
		assert Cloth.handle_claims(claims, cloth) == 4
	end

end

