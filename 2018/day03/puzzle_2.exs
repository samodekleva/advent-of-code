#! /usr/bin/env elixir

defmodule Cloth do
	def init(x \\ 1000, y \\ 1000) do
		empty_row = Map.new( for i <- 0..(x-1), do: { i, nil })
		map = Map.new(for n <- 0..(y-1), do: { n, empty_row })
		map = Map.put(map, :x, x);
		Map.put(map, :y, y)
	end

	def handle_claims([claim | claims], cloth) do
		cloth = handle_claim(cloth, Claim.init(claim))
		handle_claims(claims, cloth)
	end

	def handle_claims([], cloth) do
		cloth
	end

	def handle_claim(cloth, claim) do
		new_cloth = Map.new(for i <- 0..(cloth.y-1), do: { i, if ((i >= claim.y) && (i < (claim.y + claim.h))) do
					handle_claim_row(Map.get(cloth, i), cloth.x, claim)
				else
					Map.get(cloth, i)
				end
			})
		new_cloth = Map.put(new_cloth, :x, cloth.x)
		Map.put(new_cloth, :y, cloth.y)
	end

	def to_list(cloth) do
		for i <- 0..(cloth.x-1) do
		  Map.values(Map.get(cloth, i))
		end
	end

	def count(c, cloth) do
		to_list(cloth)
		|> Enum.map(fn x -> Enum.count(x, fn i -> (i == c) end) end)
		|> Enum.sum()
	end

	defp handle_claim_row(row, x, claim) do
	 	Map.new(for n <- 0.. (x-1) do
	 		if ((n >= claim.x ) and (n < (claim.x + claim.w))) do
				if (Map.get(row, n) == nil) do
		 			{ n, claim.id}
				else
					{ n, "X" }
		 		end
	 		else
	 			{ n, Map.get(row, n)}
	 		end
	 	end)
	end

end

defmodule Claim do
	def init(str) do
		matches = Regex.named_captures(~r/#(?<id>\d*)\s@\s(?<x>\d*),(?<y>\d*)\:\s(?<w>\d*)x(?<h>\d*)/, str)
		claim = Map.new()
		claim = Map.put_new(claim, :id, String.to_integer(matches["id"]))
		claim = Map.put_new(claim, :x, String.to_integer(matches["x"]))
		claim = Map.put_new(claim, :y, String.to_integer(matches["y"]))
		claim = Map.put_new(claim, :w, String.to_integer(matches["w"]))
		claim = Map.put_new(claim, :h, String.to_integer(matches["h"]))
		claim
	end
end

claims = File.read!("input.txt")
|> String.split("\n")

ids = Enum.map(claims, fn x ->
      Claim.init(x)
    end)

IO.inspect(ids)

cloth = File.read!("input.txt")
|> String.split("\n")
|> Enum.filter(fn x -> x <> "\n" end)
|> Cloth.handle_claims(Cloth.init())

result = Enum.filter(ids, fn x ->
  (Cloth.count(x.id, cloth) == x.w * x.h)
end)

IO.inspect(result)

# ExUnit.start()

defmodule Day3Puzzle1Test do
  use ExUnit.Case

end

